@servers(['web' => 'deployer@78.46.147.241'])

@setup
$repository = 'git@gitlab.com:zayon/deantramp-flipyard.git';
$releases_dir = '/var/www/html/releases';
$app_dir = '/var/www/html';
$release = date('YmdHis');
$new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
clone_repository
run_composer
update_symlinks
grant_permissions
rename_prod_env
migrate
@endstory

@task('clone_repository')
echo 'Cloning repository'
[ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
cd {{ $new_release_dir }}
git reset --hard {{ $commit }}
@endtask

@task('run_composer')
echo "Starting deployment ({{ $release }})"
cd {{ $new_release_dir }}
composer install --prefer-dist --no-scripts -q -o
composer update
composer dump-autoload
@endtask

@task('update_symlinks')
echo 'Linking current release'
ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('grant_permissions')
echo 'Grand perms'
chmod -R 777 {{ $app_dir }}/current
@endtask

@task('rename_prod_env')
echo 'rename .env.prod'
mv {{ $app_dir }}/current/.env.prod {{ $app_dir }}/current/.env
php {{ $app_dir }}/current/artisan key:generate

php {{ $app_dir }}/current/artisan passport:keys
@endtask

@task('migrate')
php {{ $app_dir }}/current/artisan migrate
@endtask

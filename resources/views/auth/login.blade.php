@extends('layouts.app')

@section('content')
    <div>
        <v-app id="inspire" style="background-image: url('img/Banner_Schwarz_Double_Outlines.png'); background-size: 100%;">
            <v-content>
                <v-container
                    class="fill-height"
                    fluid
                >
                    <v-row
                        align="center"
                        justify="center"
                    >
                        <v-col
                            cols="12"
                            sm="8"
                            md="4"
                        >
                            <center>
                                <img class="center" src="/img/f3.svg" width="200" style="top: 26px;position: inherit;"></img>
                            </center>
                            <h3 class="mb-0">Login</h3>
                            @error('email')
                            <p>Die eingegebenen Daten stimmen nicht mit unseren überein</p>
                            @enderror
                            <form action="/login" method="post">
                                @csrf
                                <v-text-field
                                    label="Email-adresse"
                                    name="email"
                                    type="text"
                                ></v-text-field>
                                <v-text-field
                                    label="Passwort"
                                    name="password"
                                    type="password"
                                ></v-text-field>
                                <v-btn type="submit">Login</v-btn>
                            </form>
                        </v-col>
                    </v-row>
                </v-container>
            </v-content>
        </v-app>
    </div>
@endsection

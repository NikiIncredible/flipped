<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Flipped</title>

    <link rel="manifest" href="/manifest.webmanifest">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lexend+Deca&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/d5d106270b.js"></script>
    <link rel="manifest" href="{{ asset('manifest.webmanifest') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        *:not(i), .v-application .headline, .v-application .title, .title {
            font-family: 'Lexend Deca', sans-serif!important;
        }
    </style>
</head>
<body>
<div id="app">
    @yield('content')
</div>

</body>
</html>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:500,700&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/d5d106270b.js" crossorigin="anonymous"></script>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Main meta Tags -->

    <title>Flippd - Finde Veranstaltungen in deiner nähe</title>

    <meta name="revisit-after" content="2 days" />
    <meta name="description" content="Finde Veranstaltungen in deiner nähe order erstelle deine eigene Veranstaltungen mit nur ein paar Klicks." />
    <meta name="keywords" content="Veranstaltung,flippd,deantramp,bonnytrash,flohmarkt,party,festival," />

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ url('/') }}">
    <meta property="og:title" content="Flippd - Finde Veranstaltungen in deiner nähe">
    <meta property="og:description" content="Finde Veranstaltungen in deiner nähe order erstelle deine eigene Veranstaltungen mit nur ein paar Klicks.">
    <meta property="og:image" content="{{ url('img/Banner_Schwarz_Double_Outlines.png') }}">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="{{ url('/') }}">
    <meta property="twitter:title" content="Flippd - Finde Veranstaltungen in deiner nähe">
    <meta property="twitter:description" content="Finde Veranstaltungen in deiner nähe order erstelle deine eigene Veranstaltungen mit nur ein paar Klicks.">
    <meta property="twitter:image" content="{{ url('img/Banner_Schwarz_Double_Outlines.png') }}">

    <style>
        *:not(.far):not(.fas):not(.fal):not(.fab), .v-application .headline, .v-application .title, .title {
            font-family: 'Quicksand', sans-serif!important;
        }
        h1, h2, h3, h4, h5, h6 {
            font-weight: 600;
        }
        body {
            margin-bottom: 0px!important;
        }
    </style>
</head>
<body>
<div id="app">
    <app></app>
</div>

</body>
</html>

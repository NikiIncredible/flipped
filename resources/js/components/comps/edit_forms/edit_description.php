<template>
    <form>
        <v-row v-if="change == false">
            <v-col cols="12">
                <h4 class="text">{{ getMarketById(id).description }}</h4>
                <!--                <span class><a @click="change = true">Bearbeiten</a></span>-->
            </v-col>


        </v-row>
        <v-row v-if="change">
            <v-col cols="12" md="6">
                <v-text-area
                    label="Beschreibung"
                    name="name"
                    type="name"
                    v-model="name"
                    required
                ></v-text-area>
            </v-col>
            <v-col cols="12" class="py-0">
                <v-list color="transparent" dense disabled v-if="validationErrors">
                    <v-list-item-group color="red">
                        <template v-for="(field, i) in validationErrors">
                            <v-list-item v-for="(error, i) in field" :key="i">
                                <v-list-item-icon>
                                    <v-icon color="red"> fal fa-times</v-icon>
                                </v-list-item-icon>
                                <v-list-item-content>
                                    <v-list-item-title color="red" v-html="error"></v-list-item-title>
                                </v-list-item-content>
                            </v-list-item>
                        </template>
                    </v-list-item-group>
                </v-list>
            </v-col>
            <v-col cols="12" md="12">
                <v-btn color="black" dark @click="validate" style="text-transform: none">Speichern</v-btn>
                <v-btn color="white" @click="change = false" style="text-transform: none">Abbrechen</v-btn>
            </v-col>
        </v-row>
    </form>
</template>
<script>

    import { mapGetters, mapActions } from 'vuex';

    export default {
        props: ['id'],
        computed: mapGetters(['getMarketById']),
        data: () => ({
            name: null,
            validationErrors: null,
            change: false,
        }),
        methods: {
            ...mapActions(['select']),
            validate() {
                this.validationErrors = null;
                this.formLoading = true;
                if (this.email == null || this.email == '') {
                    this.validationErrors = [['Bitte fülle die benötigten Felder aus!']];
                    this.formLoading = false;
                    return;
                } else if (this.email != this.email2) {
                    this.validationErrors = [['Die E-Mail Adressen müssen überein stimmen!']];
                    this.formLoading = false;
                    return;
                }

                this.sendForm();
            },
            sendForm() {
                if(this.editEmail(this.email)) {
                    this.formLoading = false;
                    this.change = false;

                    this.email = '';
                    this.email2 = '';
                }
            },
        },
        mounted() {
        },
    }
</script>

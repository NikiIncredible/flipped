require('./bootstrap');

import vuetify from './vuetify.js'
import VueRouter from 'vue-router'
import store from './store'

//https://peachscript.github.io/vue-infinite-loading/guide/
import InfiniteLoading from 'vue-infinite-loading';

//https://www.npmjs.com/package/vue-debounce
import vueDebounce from 'vue-debounce'

//https://www.npmjs.com/package/vue-full-calendar
import FullCalendar from 'vue-full-calendar'

import 'vueditor/dist/style/vueditor.min.css'
import 'fullcalendar/dist/fullcalendar.css'

window.Vue = require('vue');

Vue.use(InfiniteLoading, { /* options */});
Vue.use(VueRouter);
Vue.use(vueDebounce);
Vue.use(FullCalendar);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('app', require('./components/App.vue').default);

Vue.component('feed-card', require('./components/comps/feed-card.vue').default);
Vue.component('base-card', require('./components/comps/BaseCard.vue').default);
Vue.component('site-nav', require('./components/comps/nav.vue').default);
Vue.component('address-form', require('./components/comps/addressForm.vue').default);
Vue.component('email-form', require('./components/comps/email-form.vue').default);
Vue.component('password-form', require('./components/comps/password-form.vue').default);

// EDIT FORM
Vue.component('form-market-name', require('./components/comps/edit_forms/edit_name.vue').default);
Vue.component('market-overview', require("./components/sites/event/event-settings.vue").default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const routes = [
    {
        path: '/',
        name: 'Landingpage',
        component: require("./components/sites/start.vue").default,
    },
    {
        path: '/detail/*',
        name: 'Detailpage',
        component: require("./components/sites/detail.vue").default,
    },
    {
        path: '/login',
        name: 'Login',
        component: require("./components/Auth/login.vue").default,
    },
    {
        path: '/register',
        name: 'Regestrierung',
        component: require("./components/Auth/regsiter.vue").default,
    },
    // {
    //     path: '/home',
    //     name: 'Home',
    //     component: require("./components/sites/home.vue").default,
    // },
    {
        path: '/impressum',
        name: 'impressum',
        component: require("./components/sites/impressum.vue").default,
    },

    {
        path: '/home',
        component: require("./components/comps/dash_layout.vue").default,
        children: [
            {
                name: 'Home',
                path: '',
                component: require("./components/sites/home.vue").default,
            },
            {
                name: 'Notes',
                path: 'notes',
                component: require("./components/sites/home/notes.vue").default,
            },
            {
                path: 'settings',
                component: require("./components/sites/settings/user-settings.vue").default,
            },
            {
                path: 'settings/personal-info',
                component: require("./components/sites/settings/personal-info.vue").default
            },
            {
                path: 'settings/account',
                component: require("./components/sites/settings/account-info.vue").default
            },
            {
                path: 'settings/profile',
                component: require("./components/sites/settings/profile_settings.vue").default
            },
            {
                path: 'events',
                component: require("./components/sites/event/event-settings.vue").default,
            },
            {
                path: 'events/add',
                name: 'Add',
                component: require("./components/sites/event/add.vue").default,
            },
            {
                name:'event-manage',
                path: 'events/manage',
                component: require("./components/sites/event/manage.vue").default,
            },
            {
                path: 'events/manage/*',
                component: require("./components/sites/event/edit.vue").default,
            },
        ]
    }
];


const router = new VueRouter({routes, mode: 'history'});

const app = new Vue({
    el: '#app',
    router,
    vuetify,
    store,
});



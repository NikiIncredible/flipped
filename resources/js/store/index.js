import Vuex from 'vuex'
import Vue from 'vue'

import user from './modules/user'
import likes from './modules/likes'
import markets from './modules/markets'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        user,
        likes,
        markets
    }
})

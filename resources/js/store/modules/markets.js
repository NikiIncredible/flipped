const state = {
    markets: [],
    selected: null
};

const getters = {
    getAllMarkets: (state) => state,
    getMarketById: (state) => (id) => {
        return state.markets.find(market => market.id == id);
    },
    selected: (state) => state.selected
};

const actions = {
    async selectMarket({commit,getters}, id) {
        console.log(getters);
        console.log(getters.getMarketById(id));
        commit('setSelected', getters.getMarketById(id));
    },
    async fetchMarkets({commit}) {
        const response = await axios.get('/api/user/markets');
        commit('setMarkets', response.data);
        // commit('setLikeId', response.data);
    },
};

const mutations = {
    setSelected: function(state, market) {
      state.selected = market;
    },
    setMarkets: function (state, markets) {
        state.markets = markets;
    },
    setLikeId: function (state, likes) {
        var arr = [];
        likes.foreach(function (l) {
            arr.push(l.id)
        });
        state.likedID = arr;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};

const state = {
    liked: [],
    likedID: [],
};

const getters = {
    getAllLikes: (state) => state,
    checkLike: (state) => (id) => {
        var found = false;
        for (var i = 0; i < state.liked.length; i++) {
            if (state.liked[i].id == id) {
                found = true;
                break;
            }
        }
        return found
    },
};

const actions = {
    async fetchLikes({commit}) {
        const response = await axios.get('/api/user/likes');
        commit('setLikes', response.data);
        // commit('setLikeId', response.data);
    },
    async changeLike({commit, dispatch}, id) {
        const response = await axios.post('/api/user/likes/change/' + id).catch(function (error) { return });
        dispatch('fetchLikes');
    },
};

const mutations = {
    setLikes: function (state, likes) {
        state.liked = likes;
    },
    setLikeId: function (state, likes) {
        var arr = [];
        likes.foreach(function (l) {
            arr.push(l.id)
        });
        state.likedID = arr;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};

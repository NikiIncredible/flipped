const state = {
    loggedIn: false,
    isOrganizer: false,
    address: null,
    profile: {
        profileImg: '#000000',
    },
    email: null,
    name: 'Musteruser',
    id: null,
    note: "",
};

const getters = {
    getUser: (state) => state,
    getNote: (state) => state.note,
    getProfile: (state) => state.profile,
    getProfileImg: (state) => state.profile.profileImg,
    isLoggedIn: (state) => state.loggedIn,
    isOrganizer: (state) => state.isOrganizer,
};

const actions = {
    async fetchUser({commit}) {
        const response = await axios.get('/api/user');
        // console.log(response.data.data);
        commit('setUser', response.data);
    },
    async fetchNote({commit}) {
        const response = await axios.get('/api/user/note');
        if(response != null) {
            commit('setNote', response.data.data.text);
        }
    },
    async editNote({commit}, newNote) {
        const response = await axios({
            method: 'post',
            url: '/api/user/note',
            data: {
                note: newNote,
            },
        });

        commit('setNote', newNote);
        return true;
    },
    async editEmail({commit}, newEmail) {
        const response = await axios({
            method: 'post',
            url: '/api/user/cEmail',
            data: {
                email: newEmail,
            },
        });

        commit('setEmail', response.data.data.newMail);
        return true;
    },
    async editAddress({commit}, newAddress) {
        const response = await axios({
            method: 'post',
            url: '/api/user/address',
            data: {
                firstname: newAddress.firstname,
                lastname: newAddress.lastname,
                company: newAddress.company,
                street: newAddress.street,
                housenumber: newAddress.number,
                zip: newAddress.zip,
                city: newAddress.city,
                country: newAddress.country,
            },
        });
    }
};

const mutations = {
    setUser: function (state, user) {
        let isO = false;
        if (user.data.is_organizer === 1) {
            isO = true;
        }

        if (user.status == false) {
            state.loggedIn = false;
        } else {
            state.address = JSON.parse(user.data.address);
            state.email = user.data.email;
            state.name = user.data.name;
            state.id = user.data.id;
            state.profile = JSON.parse(user.data.profile),
                state.loggedIn = true;
            state.isOrganizer = isO;
        }
    },
    setEmail(state, newEmail) {
        state.email = newEmail;
    },
    setAddress(state, newAdress) {

    },
    setNote: function (state, note) {
        if (note.status === false) {

        } else {
            state.note = note;
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};

<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'accepted'             => ':Attribute muss akzeptiert werden.',
    'active_url'           => ':Attribute ist keine korrekte URL.',
    'after'                => ':Attribute muss ein Datum nach dem :date sein.',
    'after_or_equal'       => ':Attribute muss ein Datum nach dem oder am :date sein.',
    'alpha'                => ':Attribute darf nur Buchstaben enthalten.',
    'alpha_dash'           => ':Attribute darf nur Buchstaben, Zahlen und Bindestriche enthalten.',
    'alpha_num'            => ':Attribute darf nur Buchstaben und Zahlen enthalten.',
    'array'                => ':Attribute muss eine Liste sein.',
    'before'               => ':Attribute muss ein Datum vor dem :date sein.',
    'before_or_equal'      => ':Attribute muss ein Datum vor dem oder am :date sein.',
    'between'              => [
        'numeric' => ':Attribute muss zwischen :min und :max sein.',
        'file'    => ':Attribute muss zwischen :min und :max Kilobytes sein.',
        'string'  => ':Attribute muss zwischen :min und :max Zeichen sein.',
        'array'   => ':Attribute muss zwischen :min und :max Eintr&auml;ge haben.',
    ],
    'boolean'              => ':Attribute muss wahr oder falsch sein.',
    'confirmed'            => 'Die :Attribute-Best&auml;tigung stimmt nicht &uuml;berein.',
    'date'                 => ':Attribute ist kein g&uuml;ltiges Datum.',
    'date_format'          => ':Attribute entspricht nicht dem Format: :format.',
    'different'            => ':Attribute und :other m&uuml;ssen verschieden sein.',
    'digits'               => ':Attribute muss :digits Ziffern lang sein.',
    'digits_between'       => ':Attribute muss zwischen :min und :max Ziffern lang sein.',
    'dimensions'           => ':Attribute hat inkorrekte Bild-Dimensionen.',
    'distinct'             => ':Attribute hat einen doppelten Wert.',
    'email'                => ':Attribute muss eine korrekte E-Mail-Adresse sein.',
    'exists'               => 'Ausgew&auml;hlte(s) :Attribute ist inkorrekt.',
    'file'                 => ':Attribute muss eine Datei sein.',
    'filled'               => ':Attribute muss ausgef&uuml;llt werden.',
    'image'                => ':Attribute muss ein Bild sein.',
    'in'                   => 'Ausgew&auml;hlte(s) :Attribute ist inkorrekt.',
    'in_array'             => ':Attribute existiert nicht in :other.',
    'integer'              => ':Attribute muss eine Ganzzahl sein.',
    'ip'                   => ':Attribute muss eine korrekte IP-Adresse sein.',
    'ipv4'                 => ':Attribute muss eine korrekte IPv4-Adresse sein.',
    'ipv6'                 => ':Attribute muss eine korrekte IPv6-Adresse sein.',
    'json'                 => ':Attribute muss ein korrekter JSON-String sein.',
    'max'                  => [
        'numeric' => ':Attribute darf nicht gr&ouml;&szlig;er als :max sein.',
        'file'    => ':Attribute darf nicht gr&ouml;&szlig;er als :max Kilobytes sein.',
        'string'  => ':Attribute darf nicht l&auml;nger als :max Zeichen sein.',
        'array'   => ':Attribute darf nicht mehr als :max Eintr&auml;ge enthalten.',
    ],
    'mimes'                => ':Attribute muss eine Datei in folgendem Format sein: :values.',
    'mimetypes'            => ':Attribute muss eine Datei in folgendem Format sein: :values.',
    'min'                  => [
        'numeric' => ':Attribute muss mindestens :min sein.',
        'file'    => ':Attribute muss mindestens :min Kilobytes gro&szlig; sein.',
        'string'  => ':Attribute muss mindestens :min Zeichen lang sein.',
        'array'   => ':Attribute muss mindestens :min Eintr&auml;ge haben..',
    ],
    'not_in'               => 'Ausgew&auml;hlte(s) :Attribute ist inkorrekt.',
    'numeric'              => ':Attribute muss eine Zahl sein.',
    'present'              => ':Attribute muss vorhanden sein.',
    'regex'                => 'Das :Attribute-Format ist inkorrekt.',
    'required'             => ':Attribute field wird ben&ouml;tigt.',
    'required_if'          => ':Attribute field wird ben&ouml;tigt wenn :other einen Wert von :value hat.',
    'required_unless'      => ':Attribute field wird ben&ouml;tigt au&szlig;er :other ist in den Werten :values enthalten.',
    'required_with'        => ':Attribute field wird ben&ouml;tigt wenn :values vorhanden ist.',
    'required_with_all'    => ':Attribute field wird ben&ouml;tigt wenn :values vorhanden ist.',
    'required_without'     => ':Attribute field wird ben&ouml;tigt wenn :values nicht vorhanden ist.',
    'required_without_all' => ':Attribute field wird ben&ouml;tigt wenn keine der Werte :values vorhanden ist.',
    'same'                 => ':Attribute und :other m&uuml;ssen gleich sein.',
    'size'                 => [
        'numeric' => ':Attribute muss :size gro&szlig; sein.',
        'file'    => ':Attribute muss :size Kilobytes gro&szlig; sein.',
        'string'  => ':Attribute muss :size Zeichen lang sein.',
        'array'   => ':Attribute muss :size Eintr&auml;ge enthalten.',
    ],
    'string'               => ':Attribute muss Text sein.',
    'timezone'             => ':Attribute muss eine korrekte Zeitzone sein.',
    'unique'               => ':Attribute wurde bereits verwendet.',
    'uploaded'             => 'Der Upload von :Attribute schlug fehl.',
    'url'                  => 'Das :Attribute-Format ist inkorrekt.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes' => [
        'name' => 'Anzeigename',
        'email' => 'E-Mail Adresse',
        'password' => 'Passwort',
        'password_confirmation' => 'Passwort-Bestätigung',
        'remember' => 'Zugangsdaten merken',
        'name' => 'Name',
    ],
];

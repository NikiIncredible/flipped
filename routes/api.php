<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/getNewestMarkets', 'MarketController@getNewestMarkets');
Route::get('/getMarket/{id}', 'MarketController@getMarket');


Route::post('/login', 'AuthController@authenticate');
Route::post('/user/address', 'UserController@setAddress');
Route::post('register', 'AuthController@register');


Route::get('/getMarkets', 'MarketController@getAllMarketsInRadius');
Route::get('/getWeather/{city}', 'MarketController@getWeather');
Route::middleware('auth:api')->post('/markets/add', 'MarketController@addMarket');

Route::middleware('auth:api')->get('/user', 'UserController@getUser');
Route::middleware('auth:api')->get('/user/note', 'UserController@getNote');
Route::middleware('auth:api')->post('/user/note', 'UserController@postNote');
Route::middleware('auth:api')->get('/user/likes', 'UserController@getLikedMarkets');
Route::middleware('auth:api')->post('/user/likes/change/{id}', 'UserController@changeLike');
Route::middleware('auth:api')->get('/user/markets', 'UserController@getMarkets');
Route::middleware('auth:api')->post('/user/cEmail', 'UserController@changeMail');
Route::middleware('auth:api')->post('/user/cPassword', 'AuthController@changePassword');

Route::get('success', function () {
    return response()->json([
        'status' => true,
        'data' => 'success',
    ], 200);
});

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();
//Auth::routes();
//Route::get('/login', function () {
//    return view('auth.login');
//});
//Route::get('/home', function () {
//    return view('auth.home');
//})->middleware('auth');



Route::get('/{any}', function () {
    return view('welcome');
});


Route::view('/home/notes', 'welcome');
Route::view('/home/settings', 'welcome');
Route::view('/home/settings/personal-info', 'welcome');
Route::view('/home/settings/account', 'welcome');
Route::view('/home/settings/profile', 'welcome');

Route::view('/home/events', 'welcome');
Route::view('/home/events/{any}', 'welcome');
Route::view('/home/events/manage/{any}', 'welcome');

Route::get('/detail/{id}', function () {
    return view('welcome');
});
Route::get('/home', 'HomeController@index')->name('home');



Route::post('login', 'Auth\LoginController@login');
Route::middleware('auth')->get('logout', 'AuthController@logout');

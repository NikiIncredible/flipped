<?php

namespace App\Http\Controllers;

use App\Market;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MarketController extends Controller
{
    public static function getNewestMarkets(Request $request)
    {
        $markets = Market::select('id', 'adressObject', 'name', 'description', 'images', 'times', 'tags', 'features', 'sponsored', 'user', 'created_at', 'updated_at')->orderBy('created_at', 'desc')->paginate(15);
        return $markets;
    }

    public static function getMarket(Request $request)
    {

        $markets = Market::select('id', 'adressObject', 'name', 'description', 'images', 'times', 'tags', 'features', 'sponsored', 'user', 'created_at', 'updated_at')->find($request->id);
        $user = User::find($markets)->first();

        $object = (object)[
            'id' => $markets->id,
            'addressObject' => $markets->adressObject,
            'name' => $markets->name,
            'description' => $markets->description,
            'images' => $markets->images,
            'times' => $markets->times,
            'tags' => $markets->tags,
            'features' => $markets->features,
            'sponsored' => $markets->sponsored,
            'user' => (object)[
                'email' => $user->email,
                'name' => $user->name,
            ],
            'likes' => $markets->liked()->get()->count(),
        ];
        $markets = json_encode($object);
        return $markets;
    }

    public static function addMarket(Request $request)
    {
        $user = Auth::user();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://geocoder.api.here.com/6.2/geocode.json?locationid=" . $request->location_id . "&jsonattributes=1&gen=9&app_id=hWIeLUFjmkN9JeBiYjAd&app_code=gIBFOKG8SPzy12xJqmiRZg",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "cache-control: no-cache"
            ),
        ));
        $data = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $response = json_decode($data);
        $result = $response->response->view[0]->result[0];
        $lat = $result->location->displayPosition->latitude;
        $long = $result->location->displayPosition->longitude;

        $times = json_decode($request->times);

        $fTimes = [];
        foreach ($times as $time) {
            $start = Carbon::parse($time->start)->timestamp;
            $end = Carbon::parse($time->end)->timestamp;
            $array = ['from' => $start, 'to' => $end];
            array_push($fTimes, (object)$array);
        }

        $imgURL = json_encode([$request->category]);

        $rand = rand(1, 4);
        if ($rand == 1) {
            $imgURL = "/img/events/fleamarket/market.png";
        } else if ($rand == 2) {
            $imgURL = "/img/events/fleamarket/Bild1.svg";
        } else if ($rand == 3) {
            $imgURL = "/img/events/fleamarket/Bild2.svg";
        } else if ($rand == 4) {
            $imgURL = "/img/events/fleamarket/Bild3.svg";
        }

//        dd($request);

        $lalal = DB::table('markets')->insert([
            'name' => $request->name,
            'adressObject' => "{\"name\":\"" . $result->location->address->street . " " . $result->location->address->houseNumber . "\",\"zip\":\"" . $result->location->address->postalCode . "\",\"city\":\"" . $result->location->address->city . "\",\"lat\":" . $lat . ",\"long\":" . $long . "}",
            'description' => $request->description,
            'centrePoint' => DB::raw("GeomFromText('POINT(" . $lat . " " . $long . ")')"),
            'images' => json_encode([$imgURL]),
            'tags' => $request->tags,
            'category' => $request->imgURL,
            'times' => json_encode($fTimes),
            'features' => json_encode($request->features),
            'sponsored' => 0,
            'user' => $user->id,
        ]);

        return response()->json([
            'status' => true,
            'data' => 'Successful',
        ], 200);
    }

    public function getAllMarketsInRadius(Request $request)
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://geocoder.api.here.com/6.2/geocode.json?locationid=" . $request->location_id . "&jsonattributes=1&gen=9&app_id=hWIeLUFjmkN9JeBiYjAd&app_code=gIBFOKG8SPzy12xJqmiRZg",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "cache-control: no-cache"
            ),
        ));
        $data = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $response = json_decode($data);

        $lat = $response->response->view[0]->result[0]->location->displayPosition->latitude;
        $long = $response->response->view[0]->result[0]->location->displayPosition->longitude;
        $radius = 50;

        DB::raw("DECLARE @point GEOGRAPHY = GeomFromText('POINT(" . $lat . " " . $long . "));");
//        DB::statement('SELECT * FROM markets a WHERE @point.STBuffer(12500).STIntersects(a.centrePoint) = 1');
        $select = DB::select("SELECT id, adressObject, name, description, images, times, tags, features, sponsored, user, created_at, updated_at from markets where ST_Distance(POINT(" . $lat . "," . $long . "), centrePoint) <= " . ($radius * 0.21371) / 69.0585729);
        return json_encode($select);

    }

    public function getWeather(Request $request)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.openweathermap.org/data/2.5/find?APPID=4cbdaf68577c863d2318e114488e99e8&lang=de&units=metric&q=" . urlencode($request->city),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "cache-control: no-cache"
            ),
        ));

        $data = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $response = $data;
        return $response;
    }
}

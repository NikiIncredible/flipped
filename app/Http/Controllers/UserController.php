<?php

namespace App\Http\Controllers;

use App\Market;
use App\note;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function getUser(Request $request)
    {

        if(!Auth::check()) {
            return response()->json([
                'status' => false,
            ], 200);
        }

        $user = Auth::user();
        return response()->json([
            'status' => true,
            'data' => $user,
        ], 200);

    }

    public function getNote(Request $request)
    {

        if(!Auth::check()) {
            return response()->json([
                'status' => false,
            ], 200);
        }

        $note = Auth::user()->note()->first();

        return response()->json([
            'status' => true,
            'data' => $note,
        ], 200);

    }

    public function changeLike(Request $request)
    {

        $user = Auth::user();
        $market = $markets = Market::find($request->id);
        if ($user->likes->contains($market)) {
            $user->likes()->detach($market);
        } else {
            $user->likes()->attach($market);
        }

        return response()->json([
            'status' => true,
        ], 200);
    }

    public function changeMail(Request $request)
    {

        $data = $request->validate([
            'email' => 'required|email',
        ]);
        $user = Auth::user();
        $user->email = $data['email'];
        $user->save();

        return response()->json([
            'status' => true,
            'data' => (object)['newMail' => $user->email],
        ], 200);

    }

    public function postNote(Request $request) {
        $data = $request->validate([
            'note' => 'required',
        ]);
        $note = Auth::user()->note()->first();
        if($note == null) {
            $note = new note();
            $note->user_id = Auth::user()->id;
            $note->text = $data['note'];
            $note->save();
        } else {
            $note->text = $data['note'];
            $note->save();
        }

        return response()->json([
            'status' => true,
            'data' => (object)['newNote' => $note->note],
        ], 200);
    }

    function getOwnMarkets(Request $request)
    {
        $user = Auth::user();
        return $user->markets->all();
    }

    function getLikedMarkets(Request $request)
    {
        $user = Auth::user();
        return $user->likes->all();
    }

    function getMarkets(Request $request)
    {
        $user = Auth::user();
        return $user->markets->all();
    }

    public function setAddress(Request $request)
    {
        $user = Auth::user();

        $data = $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'company' => 'nullable',
            'street' => 'required',
            'housenumber' => 'required',
            'zip' => 'required',
            'city' => 'required',
            'country' => 'required',
        ]);

        $user->address = json_encode((object)$data);
        $user->save();

        return response()->json([
            'status' => true,
            'data' => 'success',
        ], 200);

    }
}

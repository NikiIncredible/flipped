<?php

namespace App\Http\Controllers;

use App\Mail\Register;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, true)) {
            // Authentication passed...
            return response()->json([
                'status' => true,
                'data' => 'success',
            ], 200);

        } else {
            return response()->json([
                'status' => false,
                'data' => 'Die eingegebenen Daten stimmen nicht mit denen in unserer Datenbank überein.',
            ], 200);

        }
    }

    public function register(Request $request)
    {

        $data = $request->validate([
            'Anzeigename' => 'required|unique:users,name|max:255',
            'Email' => 'required|email|unique:users,email',
            'Passwort' => 'required|confirmed',
        ]);


        $cre = ['email' => $data['Email'], 'password' => $data['Passwort']];


//        dd($data);

        $user = new User;
        $user->name = $data['Anzeigename'];
        $user->email = $data['Email'];
        $user->password = Hash::make($data['Passwort']);
        $user->is_organizer = 0;
        $user->profile = json_encode((object)['profileImg' => '#FF9800']);

        $user->save();

        Mail::to($user)->send(new Register($user));
        Auth::attempt($cre, true);

        return response()->json([
            'status' => true,
            'data' => 'success',
        ], 200);

    }

    function changePassword(Request $request)
    {
        $data = $request->validate([
            'password_old' => 'required|',
            'password_new' => 'required|confirmed',
        ]);

        $user = Auth::user();

        if (Auth::guard('web')->attempt(['email' => $user->email, 'password' => base64_decode($data['password_old'])])) {
            $user->password = bcrypt(base64_decode($data['password_new']));
            $user->save();

            return response()->json([
                'status' => true,
                'data' => '',
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'errors' => 'Dein altes Passwort ist leider falsch.',
            ], 200);
        }
    }

    function logout(Request $request) {
        Auth::logout();
        return redirect()->route('/');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Market extends Model
{
    protected $table = 'markets';
    protected $primaryKey = 'id';
    protected $keyType = 'integer';

    public $timestamps = false;
    protected $hidden = ['centrePoint'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user');
    }

    public function liked()
    {
        return $this->belongsToMany('App\User', 'user_liked');
    }
}

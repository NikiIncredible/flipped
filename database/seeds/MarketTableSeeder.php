<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class MarketTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        DB::table('markets')->insert([
            'name' => Str::random(10),
            'adressObject' => "{\"name\":\"".$faker->streetAddress."\",\"zip\":\"".$faker->postcode."\",\"city\":\"".$faker->city."\"}",
            'description' => $faker->text($maxNbChars = 200),
            'centrePoint' => DB::raw("GeomFromText('POINT(53.476735 9.700394)')"),
            'images' => json_encode(["img/markets/".$faker->numberBetween($min = 1, $max = 3)."/01.jpg"]),
            'tags' => implode(';', $faker->words($nb = 5, $asText = false)),
            'times' => '[{"from":1570877416,"to":1570879194},{"from":1570877416,"to":1570879194},{"from":1570877416,"to":1570879194},{"from":1570877416,"to":1570879194},{"from":1570877416,"to":1570879194}]',
            'sponsored' => $faker->numberBetween($min = 0, $max = 1),
            'user' => 1,
        ]);

    }
}

//'centrePoint' => DB::raw("GeomFromText('POINT(".$faker->latitude." ".$faker->longitude.")')"),

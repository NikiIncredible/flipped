<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Markets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('markets', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('adressObject');
            $table->point('centrePoint');
//            $table->string('addressLat');
//            $table->string('addressLong');

            $table->string('name');
            $table->text('description');
            $table->text('images');
            $table->text('times')->nullable();
            $table->string('category')->nullable();
            $table->string('homepage')->nullable();


            $table->text('tags')->nullable();
            $table->text('features')->nullable();
            $table->integer('views')->default(0);

            $table->boolean('sponsored');
            $table->integer('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('markets');
    }

}
